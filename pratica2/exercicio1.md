# Objetivos

Os objetivos deste exercício são:

* Mostrar como obter a documentação do comandos, subcomandos e opções do Docker;
* Apresentar alguns comandos mais usados do Docker;
* Mostrar a diferença entre uma imagem e um contêiner;
* Mostrar como resolver problemas de conflitos de porta e nome de contêiner;
* E explicitar que um contêiner é descartável e que dados podem ser perdidos se não for mapeado um volume Docker (assunto a ser apresentado mais adiante).
* Você também verá que é possível usar uma mesma imagem Docker para criar contêineres diferentes para disponibilizar o mesmo serviço para clientes diferentes.

# Exercícios

Considerando que você já tem o pacote ``docker-ce`` instalado no seu Docker Host, obtenha a ajuda do comando ``docker``.

```bash
docker help
docker rm --help
docker run --help
```

Obtenha as informações sobre o seu Docker Host  (máquina física ou virtual na qual são executados os contêineres).

```bash
docker info
```

Baixe uma imagem do Grafana customizada pelo instrutor.

```bash
docker pull aeciopires/ubuntu-grafana:v3
```

Liste as imagens já obtidas no seu Docker Host.

```bash
docker images
```

Visualize o histórico de mudanças na imagem do ``aeciopires/ubuntu-grafana:v3``.

```bash
docker history aeciopires/ubuntu-grafana:v3
```

Inspecione (faça uma auditoria) na imagem do ``aeciopires/ubuntu-grafana:v3``.

```bash
docker inspect aeciopires/ubuntu-grafana:v3
```

Inicie um contêiner do Grafana a partir da imagem obtida anteriormente.

```bash
docker run -d -p 3000:3000 --name=ctn1 aeciopires/ubuntu-grafana:v3
```

Liste os contêineres em execução.

```bash
docker ps
```

Visualize os recursos usados pelo contêiner.

```bash
docker stats ctn1
```

Liste as portas usadas pelo contêiner ``ctn1``.

```bash
docker port ctn1
```

Tente explicar porque o Grafana está sendo acessado no navegador via localhost ou IP da máquina virtual, se o mesmo está sendo executado em um contêiner.

Visualize os logs do contêiner iniciado anteriormente.

```bash
docker logs -f ctn1
```

Configure uma regra de firewall no Google Cloud para permitir o acesso remoto a porta 3000/TCP da instância criada. Mais informações serão passadas pelo instrutor.

Acesse o Grafana através do navegador com os dados abaixo.

URL: http://IP-SERVIDOR:3000
Login: admin
Senha: admin

Liste os processos em execução de um contêiner.

```bash
docker top ID_CONTEINER
```

Digite os comandos abaixo para visualizar determinadoss processos em execução no Docker Host.

```bash
sudo su
ps aux | grep grafana
```

Consegue perceber que o contêiner é executado como um processo do Docker Host?

Consegue perceber que é possível visualizar um processo executado no contêiner a partir do Docker Host?

Seria possível fazer isso se o contêiner fosse uma máquina virtual?

Inicie outro contêiner do Grafana.

```bash
docker run -i -t -p 3000:3000 --name=ctn2 aeciopires/ubuntu-grafana:v3
```

O que aconteceu e por que?

Pare o contêiner ``ctn1``.

```bash
docker stop ctn1
```

Liste os contêineres parados.

```bash
docker ps -a
```

Inicie outro contêiner interativo do Grafana.

```bash
docker run -d -p 3001:3000 --name=ctn3 aeciopires/ubuntu-grafana:v3
```

O que aconteceu e por que?

Inicie o contêiner ``ctn1``.

```bash
docker start ctn1
```

Pare os contêineres ``ctn2`` e ``ctn3``

```bash
docker stop ctn2 ctn3
```

Liste os contêineres parados.

```bash
docker ps -a
```

Remova os contêineres ``ctn2`` e ``ctn3``

```bash
docker rm ctn2 ctn3
```

Encerre o processo do grafana executado no contêiner `ctn1` com o comando abaixo trocando o termo PID pelo número apresentado na segunda coluna no resultado do comando ``ps aux | grep grafana``.

```bash
ps aux | grep grafana
kill -9 PID
```



