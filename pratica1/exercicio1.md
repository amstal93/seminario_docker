# Google Cloud

Crie uma instância padrão no Google Cloud com as especificações abaixo. Orientações mais precisas serão passadas pelo instrutor.

* CPU: 1 vCPU;
* Memória: aproximadamente 4 GB;
* HD: 8 a 10 GB;
* Sistema Operacional: Ubuntu Server 20.04;

> Este e outros exercícios práticos também podem ser executados em uma máquina virtual ou notebook/computador local desde que esteja utilizando o Ubuntu Desktop/Server 20.04. O uso do Google Cloud é pra suprir as limitações de instalação de pacotes e configurações de serviços presentes nos computadores do laboratório.

# Docker-CE

Instale o Docker-CE seguindo as instruções abaixo.

* No Ubuntu 20.04:

```bash
sudo apt update

sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

sudo apt update

sudo apt install docker-ce docker-ce-cli containerd.io

# Inicie o serviço Docker
sudo systemctl start docker

# Configure o Docker para ser inicializado no boot do S.O
sudo systemctl enable docker

# Adicione o seu usuário ao grupo Docker
sudo apt install acl

sudo usermod -aG docker $USER

sudo setfacl -m user:$USER:rw /var/run/docker.sock

docker --version
```

Fonte:
* https://docs.docker.com/engine/install/ubuntu/
* https://docs.docker.com/engine/install/linux-postinstall/
